<?php
session_start();
if(!isset($_SESSION['login_user'])) { // jika session login_user tidak ada atau belum terdaftar
    header("location: ../authentikasi/login"); // arahkan ke halaman login
}
require '../library/navmenu/header_admin.php';
require '../library/config.php';
$navActive='pengurus';
?>

        <div class="content-body"><!-- Basic Tables start -->
<div class="row">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Manajemen Data Pengurus NU Ranting Desa Keleng/h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <a href="<?php echo $config['web']['url'];?>ajax/pengurus/tambah " class="btn btn-success">Tambah Kader Pengurus</a>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Foto</th>
                                    <th>Nama</th>
                                    <th>Alamat</th>
                                    <th>Jabatan</th>
                                    <th>Masa Bakti</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM pengurus");
                                $no = 1;
                                while ($data = mysqli_fetch_array($query)) {
                                    ?>
                                <tr>
                                    <td><?php echo $data['id_pengurus']; ?></td>
                                    <td><?php echo $data['foto_pengurus']; ?></td>
                                    <td><?php echo $data['nama_pengurus']; ?></td>
                                    <td><?php echo $data['alamat_pengurus']; ?></td>
                                    <td><?php echo $data['jabatan_pengurus']; ?></td>
                                    <td><?php echo $data['masa_bakti_pengurus']; ?></td>
                                    <td align="center">
                                        <a href="javascript:;" onclick="users('<?php echo $config['web']['url'];?>administrator/ajax/pengguna/view?id=<?php echo $data_pengguna['id']; ?>')" class="btn btn-sm btn-info"><i class="icon-eye4"></i></a>
                                        <a href="javascript:;" onclick="users('<?php echo $config['web']['url'];?>administrator/ajax/pengguna/edit?id=<?php echo $data_pengguna['id']; ?>')" class="btn btn-sm btn-warning"><i class="icon-edit2"></i></i></a>
                                        <a href="javascript:;" onclick="users('<?php echo $config['web']['url'];?>administrator/ajax/pengguna/delete?id=<?php echo $data_pengguna['id']; ?>')" class="btn btn-sm btn-danger"><i class="icon-trash2"></i></a>
                                    </td>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- Basic Tables end -->