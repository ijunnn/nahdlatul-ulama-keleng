<?php
session_start();
if(!isset($_SESSION['login_user'])) { // jika session login_user tidak ada atau belum terdaftar
    header("location: ../authentikasi/login"); // arahkan ke halaman login
}
require '../library/navmenu/header_admin.php';
require '../library/config.php';
$navActive='pengurus';

if (isset($_POST['tambah'])) {
    $judulacara = $conn->real_escape_string($_POST['judul_acara']);
    $isiacara = $conn->real_escape_string($_POST['isi_acara']);
    $slugacara = $conn->real_escape_string($_POST['slug_acara']);
    $tglacara = $conn->real_escape_string($_POST['tgl_acara']);
    $mulaiacara = $conn->real_escape_string($_POST['mulai_acara']);
    $selesaiacara = $conn->real_escape_string($_POST['selesai_acara']);
    $tempatacara = $conn->real_escape_string($_POST['tempat_acara']);

    if ($judulacara || !$isiacara || $slugacara || $tglacara || $mulaiacara || $selesaiacara || $tempatacara) {
        $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan gagal!', 'pesan' => 'Silahkan Lengkapi Keseluruhan Datanya Coyy!');
    } 
    if ($conn->query("INSERT INTO acara VALUES('', '$judulacara', '$isiacara', '$slugacara', '$tglacara', '$mulaiacara', $selesaiacara', $tempatacara')") == true) {
        $_SESSION['hasil'] = array(
            'alert' => 'Sukses',
            'Pesan' => 'Acara Baru Telah Dibuat, Silahkan Menikmatinya'
        );
    } else {
        $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan Gagal Bro', 'pesan' => 'Permintaan Gagal!!!');
    }
} 
?>

        <div class="content-body"><!-- Basic Tables start -->
<div class="row">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Manajemen Data Acara NU Ranting Desa Keleng</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-12">
        <div class="modal fade bs-example-modal-lg" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title m-t-0"><i class="mdi mdi-account"></i> Tambah Acara</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" role="form" method="POST">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Judul Acara</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="judul" name="judul" required>
                                </div>
                            </div>                                    
                            <div class="form-group">
                                <label class="col-md-2 control-label">Slug Acara</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="slug" name="slug" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Isi Acara</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" id="isi" name="isi" rows="5" required></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Tanggal Acara</label>
                                <div class="col-md-10">
                                    <input type="date" class="form-control" id="tgl" name="tgl" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Waktu Mulai</label>
                                <div class="col-md-10">
                                    <input type="time" class="form-control" id="mulai" name="mulai" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Waktu Selesai</label>
                                <div class="col-md-10">
                                    <input type="time" class="form-control" id="selesai" name="selesai" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Tempat Acara</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="tempat" name="tempat" required>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="reset" class="btn btn-danger btn-bordred waves-effect" data-dismiss="modal"><i class="fa fa-refresh"></i> Reset</button>
                                <button type="submit" class="btn btn-info btn-bordred waves-effect w-md waves-light" name="tambah"><i class="fa fa-plus"></i> Tambah</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <button data-toggle="modal" data-target="#addModal" class="btn btn-info btn-bordred waves-effect waves-light m-b-30"><i class="fa fa-plus"></i> Tambah</button>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Judul Acara</th>
                                    <th>Tanggal Acara</th>
                                    <th>Mulai Acara</th>
                                    <th>Selesai Acara</th>
                                    <th>Tempat Acara</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM acara");
                                $no = 1;
                                while ($data = mysqli_fetch_array($query)) {
                                    ?>
                                <tr>
                                    <td><?php echo $data['id_acara']; ?></td>
                                    <td><?php echo $data['judul_acara']; ?></td>
                                    <td><?php echo $data['tgl_acara']; ?></td>
                                    <td><?php echo $data['mulai_acara']; ?></td>
                                    <td><?php echo $data['selesai_acara']; ?></td>
                                    <td><?php echo $data['tempat_acara']; ?></td>
                                    <td align="center">
                                        <a href="javascript:;" onclick="users('<?php echo $config['web']['url'];?>ajax/acara/view?id=<?php echo $data['id_acara']; ?>')" class="btn btn-sm btn-info"><i class="icon-eye4"></i></a>
                                        <a href="javascript:;" onclick="users('<?php echo $config['web']['url'];?>ajax/acara/edit?id=<?php echo $data['id_acara']; ?>')" class="btn btn-sm btn-warning"><i class="icon-edit2"></i></i></a>
                                        <a href="javascript:;" onclick="users('<?php echo $config['web']['url'];?>ajax/acara/delete?id=<?php echo $data['id_acara']; ?>')" class="btn btn-sm btn-danger"><i class="icon-trash2"></i></a>
                                    </td>
                                    <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Basic Tables end -->
<script type="text/javascript">
    function users(url) {
        $.ajax({
            type: "GET",
            url: url,
            beforeSend: function() {
                $('#modal-detail-body').html('Sedang memuat...');
            },
            success: function(result) {
                $('#modal-detail-body').html(result);
            },
            error: function() {
                $('#modal-detail-body').html('Terjadi kesalahan.');
            }
        });
        $('#modal-detail').modal();
    }
</script> 
<div class="row">
    <div class="col-md-12">     
        <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title mt-0" id="myModalLabel"><i class="mdi mdi-account"></i> Pengguna</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="modal-detail-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>      
<?php require '../library/navmenu/footer_admin.php'; ?>