<?php
session_start();
require '../library/config.php';
require '../library/session_admin.php'; 
require '../library/navmenu/header-admin.php';

if (isset($_POST['edit'])) {
    $PostStitle = $conn->real_escape_string(filter($_POST['short_title_apps']));
    $PostTitle = $conn->real_escape_string(trim($_POST['description_apps']));

    if ($conn->query("UPDATE setting_apps SET short_title_apps = '$PostStitle', description_apps = '$PostDescWeb' WHERE id = '1'") == true) {
        $_SESSION['hasil'] = array(
            'alert' => 'success', 
            'Response' => 'Permintaan berhasil', 
            'pesan' => '
            Pengaturan Website Telah Berhasil Diubah <br />                       
            ');                    
    } else {
        $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Permintaan gagal!', 'pesan' => 'Permintaan Gagal!!');
    }
}
?>  
     <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                 <?php
            if (isset($_SESSION['hasil'])) {
                ?>
        <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?>" role="alert">
    <h4 class="alert-heading"><strong>Respon : </strong><?php echo $_SESSION['hasil']['judul'] ?><br /> <strong></h4>
        <div class="alert-body">
        Pesan : </strong> <?php echo $_SESSION['hasil']['pesan'] ?>
        </div>
    </div>
    <?php
                unset($_SESSION['hasil']);
            }
            ?>

            <?php
            $time = microtime();
            $time = explode(' ', $time);
            $time = $time[1] + $time[0];
            $start = $time;
            ?>
<!-- Page-Title -->
<div class="row">
    <div class="col-md-12">
        <br /><h2 class="text-center">Pengaturan Profile dan Diskripsi Website</h2><br/>
    </div>
</div>
<!-- End-Page-Title -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="m-t-0 header-title"><b><i class="fa fa-gears"></i>    Pengaturan Website </b></h4>                             

                <div class="table-responsive">
                    <table class="table table-striped table-bordered nowrap m-0">
                        <thead>
                            <tr>
                                <th>Short Title</th>
                                <th>Title Website</th>
                                <th>Deskripsi Website</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
$CekData = $conn->query("SELECT * FROM setting_web WHERE id = '1'"); // edit
while ($ShowData = $CekData->fetch_assoc()) {
    ?>
    <tr> 
        <td><?php echo $ShowData['short_title']; ?></td>
        <td><textarea rows="5" cols="100" name="konten" class="form-control" readonly><?php echo $ShowData['short_title_apps']; ?></textarea></td>
        <td><textarea rows="5" cols="100" name="konten" class="form-control" readonly><?php echo $ShowData['description_apps']; ?></textarea></td>
        <td align="center">
            <a href="javascript:;" onclick="users('<?php echo $config['web']['url'];?>administrator/ajax/setting-aplikasi/edit?id=1')" class="btn btn-sm btn-warning"><i data-feather='edit'></i></a>
        </td>                                 
    </tr>  
<?php } ?>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
    function users(url) {
        $.ajax({
            type: "GET",
            url: url,
            beforeSend: function() {
                $('#modal-detail-body').html('Sedang memuat...');
            },
            success: function(result) {
                $('#modal-detail-body').html(result);
            },
            error: function() {
                $('#modal-detail-body').html('Terjadi kesalahan.');
            }
        });
        $('#modal-detail').modal();
    }
</script> 
<div class="row">
    <div class="col-md-12">     
        <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title mt-0" id="myModalLabel"><i class="fa fa-gears"></i> Pengaturan Website</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="modal-detail-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require '../library/navmenu/footer-admin.php';
?>