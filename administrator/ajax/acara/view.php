<?php
session_start();
require '../../../library/config.php';

    $get_idacara = $conn->real_escape_string(filter($_GET['id_acara']));
    $cek_acara = $conn->query("SELECT * FROM acara WHERE id_acara = '$get_idacara'");
    $data_acara = $cek_acara->fetch_assoc();
    if (mysqli_num_rows($cek_acara) == 0) {
        exit("Data Tidak Ditemukan");
    }
        ?>
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" role="form" method="POST">     
                    <div class="form-group">
                        <label>ID Acara</label>
                        <input type="number" class="form-control" value="<?php echo $data_acara['id_acara']; ?>" readonly>
                    </div>                                  
                    <div class="form-group">
                        <label>Judul Acara</label>
                        <input type="text" class="form-control" value="<?php echo $data_acara['judul_acara']; ?>" readonly>
                    </div>    
                    <div class="form-group">
                        <label>Slug Acara</label>
                        <input type="text" class="form-control" value="<?php echo $data_acara['slug_acara']; ?>" readonly>
                    </div>                                    
                    <div class="form-group">
                        <label>Isi Acara</label>
                        <input type="text" class="form-control" value="<?php echo $data_acara['isi_acara']; ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Acara</label>
                        <input type="text" class="form-control" value="<?php echo $data_acara['tanggal_acara']; ?>" readonly>
                    </div>  
                    <div class="form-group">
                        <label>Waktu Mulai</label>
                        <input type="text" class="form-control" value="<?php echo $data_acara['mulai_acara']; ?>" readonly>
                    </div>         
                    <div class="form-group">
                        <label>Waktu Selesai</label>
                        <input type="text" class="form-control" value="<?php echo $data_acara['selesai_acara']; ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label>Tempat Acara</label>
                        <input type="text" class="form-control" value="<?php echo $data_acara['tempat_acara']; ?>" readonly>
                    </div>                                                                    
                </form>
            </div>
        </div>       