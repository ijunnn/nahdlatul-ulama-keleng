<?php
session_start();
if(!isset($_SESSION['login_user'])) { // jika session login_user tidak ada atau belum terdaftar
    header("location: ../../../authentikasi/login"); // arahkan ke halaman login
}
require '../../../library/navmenu/header_admin.php';
require '../../../library/config.php';
$navActive='pengurus';
?>

<?php
// Syntax untuk menambahkan data acara
if(isset($_POST['submit'])) {
    // Mendapatkan data dari form
    $judul = $_POST['judul'];
    $isi = $_POST['isi'];
    $slug = strtolower(str_replace(' ', '-', $judul));
    $tgl = $_POST['tgl'];
    $mulai = $_POST['mulai'];
    $selesai = $_POST['selesai'];
    $tempat = $_POST['tempat'];
    $tglinput = date('Y-m-d');

    // Upload foto acara
    $foto = $_FILES['foto']['name'];
    $temp = $_FILES['foto']['tmp_name'];
    $path = "uploads/".$foto;
    move_uploaded_file($temp, $path);

    // Query untuk menambahkan data ke tabel acara
    $query = "INSERT INTO acara (judul_acara, foto_acara, isi_acara, slug_acara, tgl_acara, mulai_acara, selesai_acara, tempat_acara, tglinput_acara) VALUES ('$judul', '$foto', '$isi', '$slug', '$tgl', '$mulai', '$selesai', '$tempat', '$tglinput')";
    $result = mysqli_query($conn, $query);

    if($result) {
        // Jika berhasil ditambahkan, redirect ke halaman acara
        header('Location: acara.php');
    } else {
        // Jika gagal ditambahkan, tampilkan pesan error
        echo "Data gagal ditambahkan. ".mysqli_error($conn);
    }
}
?>
	<div class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title" id="basic-layout-form-center">Tambah Acara</h4>
					<a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							<li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
							<li><a data-action="reload"><i class="icon-reload"></i></a></li>
							<li><a data-action="expand"><i class="icon-expand2"></i></a></li>
							<li><a data-action="close"><i class="icon-cross2"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="card-body collapse in">
					<div class="card-block">

						<form class="form" method="POST" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-6 offset-md-3">
									<div class="form-body">
										<div class="form-group">
											<label for="eventInput1">Judul Acara</label>
											<input type="text" class="form-control" id="judul" name="judul" required>
										</div>

										<div class="form-group">
											<label for="eventInput2">Foto Acara</label>
											<input type="file" class="form-control" id="foto" name="foto" required>
										</div>

										<div class="form-group">
											<label for="eventInput3">Isi Acara</label>
											<textarea class="form-control" id="isi" name="isi" rows="5" required></textarea>
										</div>

										<div class="form-group">
											<label for="eventInput4">Tanggal Acara</label>
											<input type="date" class="form-control" id="tgl" name="tgl" required>
										</div>

										<div class="form-group">
											<label for="eventInput5">Waktu Mulai</label>
											<input type="time" class="form-control" id="mulai" name="mulai" required>
										</div>

                                        <div class="form-group">
											<label for="eventInput5">Waktu Selesai</label>
											<input type="time" class="form-control" id="selesai" name="selesai" required>
										</div>

                                        <div class="form-group">
											<label for="eventInput5">Tempat Acara</label>
											<input type="text" class="form-control" id="tempat" name="tempat" required>
										</div>

									</div>
								</div>
							</div>

							<div class="form-actions center">
								<button type="button" class="btn btn-warning mr-1">
									<i class="icon-cross2"></i> Cancel
								</button>
								<button type="submit" name="submit" class="btn btn-primary">
									<i class="icon-check2"></i> Save
								</button>
							</div>
						</form>	

					</div>
				</div>
			</div>
		</div>
	</div>

    <?php require '../../../library/navmenu/footer_admin.php'; ?>
