<?php

// jika tombol submit ditekan
if(isset($_POST['submit'])) {
  // ambil data dari form
  $nama = $_POST['nama_pengurus'];
  $jabatan = $_POST['jabatan_pengurus'];
  $masa_bakti = $_POST['masa_bakti_pengurus'];
  $alamat = $_POST['alamat_pengurus'];
  
  // ambil nama file dan path tempat file disimpan
  $foto = $_FILES['foto_pengurus']['name'];
  $tmp = $_FILES['foto_pengurus']['tmp_name'];
  
  // pindahkan file ke folder uploads
  move_uploaded_file($tmp, "uploads/".$foto);
  
  // simpan data ke dalam tabel pengurus
  $sql = "INSERT INTO pengurus (foto, nama_pengurus, jabatan_pengurus, masa_bakti_pengurus, alamat_pengurus) VALUES ('$foto', '$nama', '$jabatan', '$masa_bakti', '$alamat')";
  $result = mysqli_query($conn, $sql);
  
  // jika berhasil disimpan
  if($result) {
    echo "Data pengurus berhasil ditambahkan.";
  } else {
    echo "Terjadi kesalahan saat menambahkan data pengurus.";
  }
}
?>

<!-- form untuk menambahkan data pengurus -->
<form method="POST" enctype="multipart/form-data">
  <label for="foto">Foto:</label>
  <input type="file" name="foto"><br>
  
  <label for="nama">Nama:</label>
  <input type="text" name="nama"><br>
  
  <label for="jabatan">Jabatan:</label>
  <input type="text" name="jabatan"><br>
  
  <label for="masa_bakti">Masa Bakti:</label>
  <input type="text" name="masa_bakti"><br>
  
  <label for="alamat">Alamat:</label>
  <textarea name="alamat"></textarea><br>
  
  <input type="submit" name="submit" value="Tambah Pengurus">
</form>
