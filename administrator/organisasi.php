<?php
session_start();
if(!isset($_SESSION['login_user'])) { // jika session login_user tidak ada atau belum terdaftar
    header("location: ../authentikasi/login"); // arahkan ke halaman login
}
require '../library/navmenu/header_admin.php';
require '../library/config.php';
$navActive='pengurus';
?>

        <div class="content-body"><!-- Basic Tables start -->
<div class="row">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Manajemen Data Organisasi NU Ranting Desa Keleng</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <a href="<?php echo $config['web']['url'];?>ajax/organiasi/tambah " class="btn btn-success">Tambah Organisasi</a>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Organisasi</th>
                                    <th>Kantor Sekertariat</th>
                                    <th>Nama Ketua</th>
                                    <th>Anggota</th>
                                    <th>Masa Bakti</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM organisasi");
                                $no = 1;
                                while ($data = mysqli_fetch_array($query)) {
                                    ?>
                                <tr>
                                    <td><?php echo $data['no']; ?></td>
                                    <td><?php echo $data['nama_organisasi']; ?></td>
                                    <td><?php echo $data['kantor_sekertariat_organisasi']; ?></td>
                                    <td><?php echo $data['nama_ketua_organisasi']; ?></td>
                                    <td><?php echo $data['anggota_organisasi']; ?></td>
                                    <td><?php echo $data['masa_bakti_organisasi']; ?></td>
                                    <td align="center">
                                        <a href="javascript:;" onclick="users('<?php echo $config['web']['url'];?>ajax/organisasi/view?id=<?php echo $data['id']; ?>')" class="btn btn-sm btn-info"><i class="icon-eye4"></i></a>
                                        <a href="javascript:;" onclick="users('<?php echo $config['web']['url'];?>ajax/organisasi/edit?id=<?php echo $data['id']; ?>')" class="btn btn-sm btn-warning"><i class="icon-edit2"></i></i></a>
                                        <a href="javascript:;" onclick="users('<?php echo $config['web']['url'];?>ajax/organisasi/delete?id=<?php echo $data['id']; ?>')" class="btn btn-sm btn-danger"><i class="icon-trash2"></i></a>
                                    </td>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- Basic Tables end -->