<?php
$host = "localhost"; // nama host database
$user = "root"; // username untuk login ke database
$password = ""; // password untuk login ke database
$database = "nukeleng"; // nama database yang ingin diakses

// melakukan koneksi ke database
$conn = mysqli_connect($host, $user, $password, $database);

// cek apakah koneksi berhasil atau tidak
if (!$conn) {
    die("Koneksi gagal: " . mysqli_connect_error());
}
echo "Koneksi berhasil";
?>
