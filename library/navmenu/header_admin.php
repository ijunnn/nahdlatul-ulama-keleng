
<?php
session_start();
if(!isset($_SESSION['login_user'])) { // jika session login_user tidak ada atau belum terdaftar
    header("location: ../authentikasi/login.php"); // arahkan ke halaman login
}
?>

<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description" content="Pusat Distributor pulsa termurah dan smm.">
    <meta name="keywords" content=pusat,distributor,smm,pulsa">
    <meta name="author" content="IJUNNN">
    <title>NU Keleng</title>
    <link rel="apple-touch-icon" href="/assets/images/log/serayu.png">
    <link rel="shortcut icon" type="image/png" href="/assets/images/logo/serayu.png">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/plugins/forms/form-validation.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/pages/page-auth.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="../../assets/css/style.css">
    <!-- END: Custom CSS-->
    
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

</head>
<!-- END: Head-->
<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="">
    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow">
        <div class="navbar-container d-flex content">
            <div class="bookmark-wrapper d-flex align-items-center">
                <ul class="nav navbar-nav d-xl-none">
                    <li class="nav-item"><a class="nav-link menu-toggle" href="javascript:void(0);"><i class="ficon" data-feather="menu"></i></a></li>
                </ul>
            </div>
            <ul class="nav navbar-nav align-items-center ml-auto">
                <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style"><i class="ficon" data-feather="moon"></i></a></li>
                <li class="nav-item dropdown dropdown-user"><a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="user-nav d-sm-flex d-none"><span class="user-name font-weight-bolder"><?php echo $data_user['nama']; ?></span><span class="user-status"><?php echo $data_user['level']; ?></span></div><span class="avatar"><img class="round" src="../../app-assets/images/logo/logo1.png" alt="avatar" height="40" width="40"><span class="avatar-status-online"></span></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user"><a class="dropdown-item" href="<?php echo $config['web']['url'];?>user"><i class="mr-50" data-feather="user"></i> Profile</a><a class="dropdown-item" href="<?php echo $config['web']['url'];?>user/pemakaian-saldo"><i class="mr-50" data-feather="shopping-cart"></i>Mutasi Saldo</a><a class="dropdown-item" href="<?php echo $config['web']['url'];?>user/log"><i class="mr-50" data-feather="check-square"></i> Riwayat Login</a>
                    <div class="dropdown-divider"></div><a class="dropdown-item" href="<?php echo $config['web']['url'];?>../authentikasi/logout" onclick="alert.notif;"><i class="mr-50" data-feather="power"></i> Logout</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
<!-- END: Header-->


<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="<?php echo $config['web']['url'];?>"><span class="brand-logo">
                <img src="<?php echo $config['web']['url'];?>app-assets/images/logo/logo-nu.png"></span>
                <h2 class="brand-text"></h2>
            </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" navigation-header"><span data-i18n="Setting Website">Menu Utama</span>
            </li>
            <li class="nav-item <?= isset($navActive) ? ( $navActive == 'administrator' ? 'active' : '' ) : '' ?>"><a class="d-flex align-items-center" href="<?php echo $config['web']['url'];?>"><i data-feather='home'></i><span class="menu-title text-truncate" data-i18n="Dashboard Admin">Dashboard </span></a>
            </li>
            <li class="nav-item <?= isset($navActive) ? ( $navActive == 'pengurus' ? 'utama' : '' ) : '' ?>"><a class="d-flex align-items-center" href="<?php echo $config['web']['url'];?>pengurus"><i data-feather='home'></i><span class="menu-title text-truncate" data-i18n="Dashboard Member">Pengurus</span></a>
            </li>
            <li class="nav-item <?= isset($navActive) ? ( $navActive == 'organisasi' ? 'active' : '' ) : '' ?>"><a class="d-flex align-items-center" href="<?php echo $config['web']['url'];?>organisasi"><i data-feather='server'></i><span class="menu-title text-truncate" data-i18n="Update Provider">Pengaturan Website</span></a>
            </li>
            <li class="nav-item <?= isset($navActive) ? ( $navActive == 'acara' ? 'active' : '' ) : '' ?>"><a class="d-flex align-items-center" href="<?php echo $config['web']['url'];?>acara"><i data-feather='book-open'></i><span class="menu-title text-truncate" data-i18n="Kelola Berita">Pengaturan Aplikasi</span></a>
            </li> 
            <li class="nav-item <?= isset($navActive) ? ( $navActive == 'artikel' ? 'active' : '' ) : '' ?>"><a class="d-flex align-items-center" href="#"><i data-feather='book-open'></i><span class="menu-title text-truncate" data-i18n="Kelola Berita">Artikel</span></a>
            <ul class="menu-content">
                <li class="nav-item"><a href="<?php echo $config['web']['url'];?>kategori" class="d-flex align-items-center">Kategori</a></li>
                <li class="nav-item"><a href="<?php echo $config['web']['url'];?>artikel" class="d-flex align-items-center">Data Artikel</a></li>
                <li class="nav-item"><a href="<?php echo $config['web']['url'];?>tambah-artikel" class="d-flex align-items-center">Tambah Artikell</a></li>
            </ul>
            </li> 
            <li class="nav-item <?= isset($navActive) ? ( $navActive == 'galeri' ? 'active' : '' ) : '' ?>"><a class="d-flex align-items-center" href="#"><i data-feather='book-open'></i><span class="menu-title text-truncate" data-i18n="Kelola Berita">Artikel</span></a>
            <ul class="menu-content">
                <li class="nav-item"><a href="<?php echo $config['web']['url'];?>foto" class="d-flex align-items-center">Foto</a></li>
                <li class="nav-item"><a href="<?php echo $config['web']['url'];?>video" class="d-flex align-items-center">Video </a></li>
            </ul>
            </li> 
            <li class="nav-item <?= isset($navActive) ? ( $navActive == 'setting-aplikasi' ? 'active' : '' ) : '' ?>"><a class="d-flex align-items-center" href="<?php echo $config['web']['url'];?>acara"><i data-feather='book-open'></i><span class="menu-title text-truncate" data-i18n="Kelola Berita">Pengaturan Aplikasi</span></a>
            </li> 
        </ul>
            
</div>
</div>
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">