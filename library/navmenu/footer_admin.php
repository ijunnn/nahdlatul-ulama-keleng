</div>
</div>
</div>
</div>
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<footer class="footer footer-static footer-light text-center">
    <p class="clearfix mb-0">&copy; 2022 <b>Serayu Store.</b> Crafted with <i class="fas fa-heart text-danger"></i> by <a href="https://ijunproject.com" target="_blank"><b>IjunProject.</b></a></p>
</footer>
<style>
    menu {
  --size: 2.7rem;
  --radius: 6rem;
  --padding: 1rem;
  --bg-color: rgba(26, 160, 237, 1);
  --fg-color: rgba(227,244,253, 1);
  --hi-color: #ffffff;
  position: fixed;
  bottom: var(--padding);
  right: var(--padding);
}
menu > * {
  position: absolute;
  display: grid;
  place-content: center;
  border-radius: 50%;
  background: var(--bg-color);
  color: var(--fg-color);
  text-decoration: none;
  box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.4);
}
menu > .action {
  --factor: 0;
  width: 4rem;
  height: 4rem;
  right: calc(0.35 * var(--size));
  bottom: calc(0.35 * var(--size));
  opacity: 0;
  transform: rotate(calc(-1 * var(--angle)))
    translateY(calc(-1 * var(--radius) * var(--factor))) rotate(var(--angle));
  transition: transform 250ms ease-in-out, opacity 250ms ease-in-out,
    box-shadow 250ms ease-in-out, color 250ms ease-in-out;
}
menu > .action:hover,
menu > .trigger:hover {
  color: var(--hi-color);
  box-shadow: 0 0 0 0.25rem rgba(26, 160, 237, 0.3);
}
menu.open > .action {
  --factor: 1;
  opacity: 1;
}
menu > .action:nth-child(1) {
  --angle: 0deg;
  transition-delay: 0s;
  font-size: 2rem;
}
menu > .action:nth-child(2) {
  --angle: 45deg;
  transition-delay: 50ms;
  font-size: 2rem;
}
menu > .action:nth-child(3) {
  --angle: 90deg;
  transition-delay: 0.1s;
  font-size: 2rem;
}
menu > .trigger {
  width: calc(1.6 * var(--size));
  height: calc(1.6 * var(--size));
  bottom: 0;
  right: 0;
  font-size: 2rem;
  transition: box-shadow 250ms ease-in-out, color 250ms ease-in-out;
}
menu > .trigger > i {
  transition: transform 250ms ease-in-out;
}
menu.open > .trigger > i {
  transform: rotate(-135deg);
}

</style>

    <!-- BEGIN: Footer-->
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="../../app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="../../app-assets/vendors/js/charts/apexcharts.min.js"></script>
    <script src="../../app-assets/vendors/js/extensions/toastr.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="../../app-assets/js/core/app-menu.js"></script>
    <script src="../../app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="../../app-assets/js/scripts/pages/dashboard-ecommerce.js"></script>
    <!-- END: Page JS-->

    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
    <script>
        jQuery(document).ready(function($){
            $('.alert_notif').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                        title: 'Alert',
                        text: 'Hapus Data?',
                        html: true,
                        confirmButtonColor: '#d9534f',
                        showCancelButton: true,
                        },function(){
                        window.location.href = getLink
                    });
                return false;
            });
        });
	 
    </script>
</body>
</html>