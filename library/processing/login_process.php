<?php
// memanggil konfigurasi database
require '../config.php';

// mengambil data dari form login
$email = $_POST['email'];
$password = $_POST['password'];

// mencari data pengguna di database
$sql = "SELECT * FROM users WHERE email='$email'";
$result = mysqli_query($conn, $sql);

// cek apakah email ditemukan
if (mysqli_num_rows($result) === 1) {
    $row = mysqli_fetch_assoc($result);
    // cek apakah password benar
    if (password_verify($password, $row['password'])) {
        // login berhasil, redirect ke halaman utama
        header("Location: ../../administrator/index.php");
        exit;
    }
}

// login gagal, redirect ke halaman login
header("Location: login.php");
exit;
?>
