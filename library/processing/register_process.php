<?php
// memanggil konfigurasi database
require '../config.php';

// mengambil data dari form daftar
$email = $_POST['email'];
$password = $_POST['password'];
$confirm_password = $_POST['confirm_password'];

// cek apakah email sudah terdaftar
$sql = "SELECT * FROM users WHERE email='$email'";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    // email sudah terdaftar, redirect ke halaman daftar
    header("Location: register.php?error=email");
    exit;
} else {
    // hash password sebelum disimpan ke database
    $hashed_password = password_hash($password, PASSWORD_DEFAULT);

    // insert data pengguna baru ke database
    $sql = "INSERT INTO users (email, password) VALUES ('$email', '$hashed_password')";
    if (mysqli_query($conn, $sql)) {
        // pendaftaran berhasil, redirect ke halaman login
        header("Location: ../../authentikasi/login.php");
        exit;
    } else {
        // pendaftaran gagal, redirect ke halaman daftar
        header("Location: register.php?error=database");
        exit;
    }
}
?>
