CREATE TABLE users (
  id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nama VARCHAR(50) NOT NULL,
  nohp VARCHAR(14) NOT NULL,
  email VARCHAR(50) NOT NULL UNIQUE,
  password VARCHAR(255) NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE pengurus (
  id_pengurus INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nama_pengurus VARCHAR(50) NOT NULL,
  jabatan_pengurus VARCHAR(50) NOT NULL,
  masa_bakti_pengurus VARCHAR(50) NOT NULL,
  alamat_pengurus VARCHAR(100) NOT NULL,
  foto_pengurus VARCHAR(100) NOT NULL
);

CREATE TABLE organisasi (
    id_organisasi INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nama_organiasi VARCHAR(255) NOT NULL,
    kantor_sekertariat_organisasi VARCHAR(255) NOT NULL,
    ketua_organisasi VARCHAR(255) NOT NULL,
    anggota_organisasi VARCHAR(100) NOT NULL,
    masa_bakti VARCHAR(100) NOT NULL,  
);

CREATE TABLE acara (
    id_acara INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    judul_acara VARCHAR(50) NOT NULL,
    foto_acara VARCHAR(50),
    isi_acara TEXT,
    slug_acara VARCHAR(50) NOT NULL,
    tgl_acara DATE,
    mulai_acara TIME,
    selesai_acara TIME,
    tempat_acara VARCHAR(50),
    tglinput_acara TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);



